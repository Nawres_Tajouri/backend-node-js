const mongoose = require('mongoose');
const { required } = require('nodemon/lib/config');

const PostSchema = mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    username : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    phone : {
        type : String,
        required : true
    },
    website : {
        type : String,
        required : true
    },

})

/* title : {
        type : String,
        required : true
        },
   description : {
        type : String,
        required : true
        }

} */

module.exports = mongoose.model('Posts', PostSchema);