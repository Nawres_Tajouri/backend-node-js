const express = require('express');
const mongoose = require('mongoose');
require('dotenv/config');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const options = {
    definition : {
        openapi: "3.0.0",
        info: {
            title: "Users API",
            version: "1.0.0",
            description: "A simple Express User API"
        },
        servers: [
            {
                url: "http://localhost:3004"
            }
        ],
    },
    apis: ['./routes/*.js'],
};

const specs = swaggerJsDoc(options)

const app = express();
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

//MIDDLEWARES
app.use(cors());
app.use(bodyParser.json());
app.use(morgan("dev")); 
//import routes
const postsRoute = require('./routes/posts');

//middleware
app.use('/users', postsRoute);

//routes
app.get('/', (req,res) => {
    res.send('I am Nawres');
});



//connect to DB
mongoose.connect( process.env.DB_CONNECTION ,
() => console.log('connected to DB') 
);

//listen to the server
app.listen(3004);