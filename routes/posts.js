const { json } = require('body-parser');
const express = require('express');
const { route } = require('express/lib/application');
const req = require('express/lib/request');
const res = require('express/lib/response');
const nanoid = require('nanoid');
const router = express.Router();
const Post = require('../models/Post');

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required: 
 *         - name
 *         - username
 *         - email
 *         - phone
 *       properties:
 *         name:
 *           type: string
 *           description: The user name
 *         username:
 *           type: string
 *           description: The user username
 *         email:
 *           type: string
 *           description: The user email
 *         phone:
 *           type: string
 *           description: The user phone
 *         website:
 *           type: string
 *           description: The user website
 *       example:
 *         name: Nawres
 *         username: Tajouri
 *         email: naourestajouri@gmail.com
 *         phone: 11111111
 *         website: nawres.com             
 */

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: The users managing API
 */

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Returns the list of all the users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: The list of all the users
 *         content: 
 *           application/json:
 *             schema:
 *               type: array
 *               items: 
 *                 $ref: '#/components/schemas/User'
 */
router.get('/', async (req,res) => {
    try{
        const posts = await Post.find();
        res.json(posts);
}catch(err){
    res.json({message:err});
}
});

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a new user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */

router.post('/', async (req,res) => {
    const post = new Post ({
        name : req.body.name,
        username : req.body.username,
        email : req.body.email,
        phone : req.body.phone,
        website : req.body.website

    });

     try{   
     const savedPost = await post.save()
        res.json(savedPost);
     }catch(err){
            return res.status(500).send(error);
            //res.json({ message: err });
        };
});

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema: 
 *           type: string
 *         required: true
 *         description: The user id
 *     responses:
 *       200:
 *         description: The user description by id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: The user was not found 
 */

router.get('/:id', async (req,res) => {
    try{
        const post= await Post.findById(req.params.id);
    //const post = Post.findById(req.params.id);
        res.json(post);
    }catch(err) {
        res.sendStatus(404);
    }; 
});

/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: Remove the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     responses:
 *       200:
 *         description: The user was deleted
 *       404:
 *         description: The user was not found
 */

router.delete('/:id', async (req,res) => {
    try{
        const postDeleted = await Post.remove({_id : req.params.id});
        res.json(postDeleted);
        res.sendStatus(200)
    }catch(err){
        res.sendStatus(404);
    }
});

/**
 * @swagger
 * /users/{id}:
 *   patch:
 *     summary: Update the user by the id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The user was updated
 *         content:
 *           application/json:
 *             schema:
 *             $ref: '#/components/schemas/User'
 *       404:
 *         description: The user was not found
 *       500:
 *         description: Some error happened
 */

router.patch('/:id', async (req,res) => {
    try{
        const updatedPost= await Post.updateOne(
            {_id : req.params.id},
           {$set: {name: req.body.name}
         });
         res.json(updatedPost);
    }catch(err){
        res.json({message : err});
    }
});


module.exports = router;
